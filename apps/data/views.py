from rest_framework import generics, permissions as perms, response, decorators, filters
from . import serializers, models, permissions
from django.shortcuts import get_object_or_404

class FoodsList(generics.ListCreateAPIView):
    permission_classes = [perms.IsAuthenticated, permissions.IsAdminOrReadOnly]
    serializer_class = serializers.FoodsSer
    queryset = models.Foods.objects.all()

class ActivitiesList(generics.ListCreateAPIView):
    permission_classes = [perms.IsAuthenticated, permissions.IsAdminOrReadOnly]
    serializer_class = serializers.ActivitiesSer
    queryset = models.Activities.objects.all()

    


@decorators.api_view(['GET'])
def search(request):
    query = request.GET.get("search", None)
    activities = models.Activities.objects.all()
    foods = models.Foods.objects.all()

    if query:
        # try:
            activities = models.Activities.objects.filter(name__icontains=query)
            foods = models.Foods.objects.filter(name__icontains=query)
        # except :
        #     return response.Response({"detail": "there is nothing to show"})
    results = {"foods":serializers.FoodsSer(instance=foods, many=True).data}
    results.update({'activities':serializers.ActivitiesSer(instance=activities,  many=True).data})
    return response.Response({"results": results})