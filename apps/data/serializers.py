from rest_framework import serializers
from .models import *

class FoodsSer(serializers.ModelSerializer):
    class Meta:
        model = Foods
        fields = '__all__'
        read_only_fields = ['diet','calorie']

class ActivitiesSer(serializers.ModelSerializer):
    class Meta:
        model = Activities
        fields = '__all__'
        read_only_fields = ['diet','calorie']

