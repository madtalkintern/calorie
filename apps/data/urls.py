from django.urls import path
from .views import *

urlpatterns = [
    path('foods/', FoodsList.as_view()),
    path('activities/', ActivitiesList.as_view()),
    path('se/', search),
]
