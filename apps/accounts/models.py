from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    class Types(models.TextChoices):
        Admin = "Admin"
        User = "User"

    type = models.CharField('type',max_length=50, choices=Types.choices)
    height = models.IntegerField('height', null=True, blank=True)
    weight = models.IntegerField('weight', null=True, blank=True)
    phone = models.CharField('phone', max_length=11, null=True)


    @property
    def full_name(self):
        return self.first_name + " " + self.last_name

# class DietUser(models.Model):
#     models.ForeignKey(User, on_delete=models.CASCADE)
    