from django.urls import path
from .views import *

urlpatterns = [

    path('list/', DietList.as_view()),
    path('detail/<int:id>/', DietDetail.as_view()),
    path('adds/<int:diet_id>/activities/', DietAddsActivities.as_view()),
    path('adds/<int:diet_id>/foods/', DietAddsFoods.as_view()),
    path('compute/calorie/', compute_calorie),
    path('compute/bmi/', compute_bmi),
]