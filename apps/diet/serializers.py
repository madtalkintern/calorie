from rest_framework import serializers
from .models import *

class DietSer(serializers.ModelSerializer):
    class Meta:
        model = Diet
        fields = '__all__'
        read_only_fields = ['admin']

class DietFoodSer(serializers.ModelSerializer):
    class Meta:
        model = FoodDiet
        fields = '__all__'
class DietActivitySer(serializers.ModelSerializer):
    class Meta:
        model = ActivityDiet
        fields = '__all__'