from django.contrib import admin
from .models import *

admin.site.register(Diet)
admin.site.register(FoodDiet)
admin.site.register(ActivityDiet)