from . import serializers, models, permissions
from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions as perms, response, decorators
from ..data.models import *

class DietList(generics.ListCreateAPIView):
    permission_classes = [perms.IsAuthenticated, permissions.IsAdminOrReadOnly]
    serializer_class = serializers.DietSer

    def get_queryset(self):
        if self.request.method == 'POST':
            return models.Diet.objects.all()
        else :
            if self.request.user.type == 'admin':
                return models.Diet.objects.filter(admin=self.request.user)
            else:
                return models.Diet.objects.filter(user=self.request.user)


    def perform_create(self, serializer):
        serializer.save(admin=self.request.user)

class DietDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [perms.IsAuthenticated, permissions.IsAdminOrReadOnly]
    serializer_class = serializers.DietSer
    lookup_field = 'id'
    def get_queryset(self):
        if self.request.method == 'GET':
            user = self.request.user
            obj = get_object_or_404(models.Diet, pk=self.kwargs['id'])
            if obj.admin == user:
                return models.Diet.objects.all()
            else:
                return models.Diet.objects.filter(user=user)
        else :
            return models.Diet.objects.all()
    
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        result = serializer.data
        food_obj = models.FoodDiet.objects.select_related('diet').filter(diet=instance.id)
        activity_obj = models.ActivityDiet.objects.select_related('diet').filter(diet=instance.id)
        foods = serializers.DietFoodSer(food_obj, many=True).data
        activities = serializers.DietActivitySer(activity_obj, many=True).data
        income_calorie = 0
        used_calorie = 0
        for food in food_obj:
            income_calorie += food.calorie 
        for activity in activity_obj:
            used_calorie += activity.calorie 
        total_calorie = income_calorie - used_calorie
        result.update({"foods":foods})
        result.update({"activities":activities})
        result.update({"daily_calorie":total_calorie})
        return response.Response(result)

class DietAddsActivities(generics.ListCreateAPIView):
    permission_classes = [perms.IsAuthenticated, permissions.IsAdminOrReadOnly]
    serializer_class = serializers.DietActivitySer

    def get_queryset(self):
        if self.request.method == 'GET':
            if self.request.user.type == 'admin':
                return models.ActivityDiet.objects.select_related('diet').filter(diet_id=self.kwargs['id'])
            else:
                models.ActivityDiet.objects.select_related('diet').filter(diet_id=self.kwargs['id'], diet_user=self.request.user.id)
        else:
            models.ActivityDiet.objects.all()
    def perform_create(self, serializer):
        serializer.save(diet=self.kwargs['id'])

class DietAddsFoods(generics.ListCreateAPIView):
    permission_classes = [perms.IsAuthenticated, permissions.IsAdminOrReadOnly]
    serializer_class = serializers.DietFoodSer

    def get_queryset(self):
        if self.request.method == 'GET':
            if self.request.user.type == 'admin':
                return models.FoodDiet.objects.select_related('diet').filter(diet_id=self.kwargs['id'])
            else:
                models.FoodDiet.objects.select_related('diet').filter(diet_id=self.kwargs['id'], diet_user=self.request.user.id)
        else:
            models.FoodDiet.objects.all()

    def perform_create(self, serializer):
        serializer.save(diet=self.kwargs['id'])

@decorators.api_view(['POST'])
def compute_calorie(request):
    foods = request.data.get('foods', None)
    activities = request.data.get('activities', None)
    if foods is None and activities is None:
        return response.Response({"details":"enter values"})
    used_calories = 0
    income_calories = 0
    if foods is not None:
        for food in foods:
            food_obj = get_object_or_404(Foods, pk=food[0])
            income_calories += (food[1] / 100)*food_obj.calorie


    if activities is not None:
        for activity in activities:
            activity_obj = get_object_or_404(Activities, pk=activity[0])
            used_calories += (activity[1] / 30)*activity_obj.calorie

    total_calorie = income_calories - used_calories

    if total_calorie >= 50 :
        return response.Response({"result":f"u gained {total_calorie}kc today, you are getting fat"})

    elif total_calorie <= 50 :
        return response.Response({"result":f"u lost {total_calorie}kc today, you are getting thin"})


    else :
        return response.Response({"result":"you are not doing anything :)"})

@decorators.api_view(['GET','POST'])
def compute_bmi(request):
    if request.method == 'POST':
        weight = request.data.get('weight')
        height = request.data.get('height')
    else:
        try:
            user = request.user
            weight = user.weight
            height = user.height
        except:
            return response.Response({"details":"login or enter your informations"})
    mh = (height/100)
    bmi = weight / (mh*mh)
    return response.Response({"result":bmi})
