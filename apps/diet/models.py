from django.db import models
from django.contrib.auth import get_user_model

class Diet(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="user")
    admin = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True,related_name="admin")

class FoodDiet(models.Model):
    diet = models.ForeignKey(Diet, on_delete=models.CASCADE, related_name="food_diet")
    food = models.ForeignKey('data.Foods', on_delete=models.CASCADE, related_name="food")
    mass = models.IntegerField()
    @property
    def calorie(self):
        return (self.mass / 100) * self.food.calorie
        
class ActivityDiet(models.Model):
    diet = models.ForeignKey(Diet, on_delete=models.CASCADE, related_name="activity_diet")
    activity = models.ForeignKey('data.Activities', on_delete=models.CASCADE, related_name="activity")
    time = models.IntegerField()
    @property
    def calorie(self):
        return (self.time / 30) * self.activity.calorie