from rest_framework import serializers
from .models import *

class RoutineFoodSer(serializers.ModelSerializer):
    class Meta:
        model = FoodRoutine
        fields = '__all__'
class RoutineActivitySer(serializers.ModelSerializer):
    class Meta:
        model = ActivityRoutine
        fields = '__all__'