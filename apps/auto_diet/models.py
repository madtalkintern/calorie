from django.db import models
from django.contrib.auth import get_user_model


class FoodRoutine(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="food_routine")
    food = models.ForeignKey('data.Foods', on_delete=models.CASCADE)
    mass = models.IntegerField()
    @property
    def calorie(self):
        return (self.mass / 100) * self.food.calorie

class ActivityRoutine(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="activity_routine")
    activity = models.ForeignKey('data.Activities', on_delete=models.CASCADE)
    time = models.IntegerField()
    @property
    def calorie(self):
        return (self.time / 30) * self.activity.calorie