from . import serializers, models, permissions
from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions as perms, response, decorators
from ..diet import models as die_models
from ..data import models as data_models
import random


class RoutineActivities(generics.ListCreateAPIView):
    permission_classes = [perms.IsAuthenticated]
    serializer_class = serializers.RoutineActivitySer
    queryset = models.ActivityRoutine
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class RoutineFoods(generics.ListCreateAPIView):
    permission_classes = [perms.IsAuthenticated]
    serializer_class = serializers.RoutineFoodSer
    queryset = models.FoodRoutine
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

@decorators.api_view(['GET'])
@decorators.permission_classes([perms.IsAuthenticated])
def auto_diet(request):
    # goal = ['loose', 'nothing', 'gain']
    param = request.GET.get('goal')
    if param is not None:
        try:
            user_goal= int(param)
        except ValueError:
            return response.Response({"details":"entered wrong param"}, status=400)
    else:
        user_goal = 0

    daily_calorie = 0
    food_routines = request.user.food_routine.all()
    for food_routine in food_routines:
        daily_calorie += food_routine.calorie
    activity_routines = request.user.activity_routine.all()
    for activity_routine in activity_routines:
        daily_calorie -= activity_routine.calorie    

    if user_goal == 0:
        loose(request, daily_calorie)
    elif user_goal == 1:
        response.Response({"result":"you dont need to do any thing, just add some exercise to your daily routine to improve your health"})
    else:
        gain(request, daily_calorie)


    return response.Response({"result":"diet added"})

def gain(request, daily_calorie,):
    diet = die_models.Diet.objects.create(user=request.user)
    user_food_list = []
    user_food_mass = []
    user_foods= request.user.food_routine.all()
    for user_food in user_foods:
        user_food_list.append(user_food.food.id)
        user_food_list.append(user_food.mass)
    qs = data_models.Foods.objects.all().exclude(id__in=user_food_list)
    if daily_calorie > 50:
        needed_food = qs.filter(calorie__lte=50)
        if len(needed_food) > 0:
            pass
        else:
            needed_food = qs.filter(calorie__lte=100)
            if len(needed_food) > 0:
                user_food_mass.append(50)
            else:
                needed_food = qs.filter(calorie__lte=200)
                user_food_mass.append(25)

    else:
        needed_food = qs.filter(calorie__gte=50)
    random_food = random.choice(needed_food)
    user_food_list.append(random_food.id)
    user_food_mass.append(100)
    for user_food_id in user_food_list:
        die_models.FoodDiet.objects.create(diet_id = diet.id, food_id=user_food_id, mass = user_food_mass[user_food_list.index(user_food_id)])

def loose(request, daily_calorie):
    diet = die_models.Diet.objects.create(user=request.user)
    diet.save()
    user_activity_list = []
    user_activity_time = []
    user_activities= request.user.activity_routine.all()
    for user_activity in user_activities:
        user_activity_list.append(user_actiget_user_modelvity.activity.id)
        user_activity_time.append(user_activity.time)
    qs = data_models.Activities.objects.all().exclude(id__in=user_activity_list)
    if daily_calorie > 50:
        needed_activity = qs.filter(calorie__lte=50)
        if len(needed_activity) > 0:
            pass
        else:
            needed_activity = qs.filter(calorie__lte=100)
            if len(needed_activity) > 0:
                user_activity_time.append(15)
            else:
                needed_activity = qs.filter(calorie__lte=200)
                user_activity_time.append(10)
    else:
        needed_activity = qs.filter(calorie__gte=50)
    random_activity = random.choice(needed_activity)
    user_activity_list.append(random_activity.id)
    user_activity_time.append(30)

    for user_activity_id in user_activity_list:

        act_die = die_models.ActivityDiet.objects.create(diet = diet, activity=data_models.Activities.objects.get(pk=user_activity_id), time = user_activity_time[user_activity_list.index(user_activity_id)])
        act_die.save()