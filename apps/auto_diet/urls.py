from django.urls import path, include
from .views import *

urlpatterns = [
    path('', auto_diet),
    path('add/activities/', RoutineActivities.as_view()),
    path('add/foods/', RoutineFoods.as_view()),
]