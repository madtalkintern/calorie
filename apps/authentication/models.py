from django.db import models
import datetime

class Otp(models.Model):
    mobile = models.CharField(max_length=11, unique=True)
    created_at = models.DateTimeField(auto_now=True)
    counter = models.PositiveIntegerField(default=0, blank=False)

    @property
    def is_expired(self):
        now = datetime.datetime.now().timestamp()
        otp_time = self.created_at.timestamp()
        diff_time = now - otp_time
        if diff_time > 90:
            return True
        return False
    
    class Meta:
        ordering = ['-id']
        verbose_name = "OTP"
        verbose_name_plural = "OTPs"