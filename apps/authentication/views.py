from rest_framework import response, status, decorators
import pyotp
from . import utils, models
import base64
from django.contrib.auth import get_user_model



@decorators.api_view(['POST'])
def otp_login(request):
    phone = request.data.get('phone')
    code = request.data.get('code', None)

    if code is not None:
        try:
            otp = models.Otp.objects.get(mobile=phone)
        except models.Otp.DoesNotExist:
            return response.Response({'detail': "wrong number"}, status=status.HTTP_400_BAD_REQUEST)
        if otp.is_expired:
            return response.Response({'detail': "expired cade"}, status=status.HTTP_400_BAD_REQUEST)
        # if len(code)<5 or len(code)>6:
        #     return response.Response({'detail': "wrong code format"}, status=status.HTTP_400_BAD_REQUEST)
        keygen = utils.GenerateKey()
        key = base64.b32encode(keygen.returnValue().encode())
        OTP = pyotp.HOTP(key)
        data = {}
        if OTP.verify(code, otp.counter):
            user = get_user_model().objects.filter(phone=phone)
            token = utils.get_tokens_for_user(user)
            data['access'] = token['access']
            data['refresh'] = token['refresh']
            response.Response({"result":data})
        return response.Response({'detail': "wrong code"}, status=status.HTTP_400_BAD_REQUEST)
    else:
        if not utils.is_valid_phone(phone):
            return response.Response({'detail': "wrong number format"},
                                     status=status.HTTP_400_BAD_REQUEST)
        if get_user_model().objects.filter(phone=phone).exists():
            pass
        else :
            return response.Response({'detail': "there is no such phone number"},
                                         status=status.HTTP_400_BAD_REQUEST)
        try:
            otp = models.Otp.objects.get(mobile=phone)
            if not otp.is_expired:
                return response.Response({'detail': "try after 90 secends"},
                                         status=status.HTTP_400_BAD_REQUEST)
        except models.Otp.DoesNotExist:
            otp = models.Otp.objects.create(mobile=phone)
        otp.counter += 1
        otp.save()
        keygen = utils.GenerateKey()
        key = base64.b32encode(keygen.returnValue().encode())
        code = pyotp.HOTP(key)
        return response.Response({"detail": "code sent"},
                                 status=status.HTTP_200_OK)